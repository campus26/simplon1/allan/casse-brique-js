import {
    barGlobal
} from "./bar.js";
import {
    engine
} from "./game.js";
import {
    jouefire
} from "./sounds.js";



let count = 3;  //nombre de tirs
export function fireBall() {
    //touche espace declanche le tir
    document.addEventListener('keydown', event => {
        if (event.code === 'Space') {
            if(count > 0){
                fire();
                count--;
            }
        }
    });
    //clique dans le canvas declanche le tir
    let elm = document.querySelector("#myCanvas");
    elm.addEventListener('click', event => {
        if (count > 0)
            fire();
            count--;
    });

}

function fire(){    //fonction qui cree l'image du tir dans le canvas
    let elm = document.querySelectorAll(".imgfire");

    elm[elm.length - 1].remove(); //enleve les images de tir du DOM à chaque tir
    let fire = Bodies.rectangle(barGlobal.position.x, 550, 10, 25, {
        isSensor: true,
        render: {
            sprite: {
                texture: 'assets/image/bullet.png',
                xScale: 0.18,
                yScale: 0.08
            }
        }
    });
    fire.friction = 0;
    fire.frictionAir = 0;
    fire.restitution = 1;
    fire.frictionStatic = 0;
    fire.inertia = Infinity;
    fire.inverseMass = 1000000; 
    fire.label = "fire";
    Body.setVelocity(fire, {
        x: 0,
        y: -5
    });
    jouefire();
    World.add(engine.world, fire);

}