import {
    engine,
    render
} from "./game.js";

function getRandomInt(max) {       //retourne un entier de 0 à max
    return Math.floor(Math.random() * Math.floor(max));
}

//la disposition des briques correspond aux tableaux groups
/*  Un groupe des 4 et choisie (random) au depart   */
let groups1 = [
    [1,0,5,0,0,0,5,0,1],
    [1,1,1,1,6,1,1,1,1],
    [1,1,1,1,4,1,1,1,1],
    [1,2,1,1,1,1,1,2,1],
    [1,1,3,1,1,1,3,1,1]
];
let groups2 = [
    [1,0,0,0,5,0,0,0,1],
    [1,1,1,1,2,1,1,1,1],
    [1,3,1,1,5,1,1,3,1],
    [1,1,1,5,6,5,1,1,1],
    [2,1,1,1,4,1,1,1,2]
];
let groups3 = [
    [1,0,0,0,0,0,0,0,1],
    [1,1,1,1,2,1,1,1,1],
    [1,1,1,1,2,1,1,1,1],
    [1,1,1,1,4,1,1,1,1],
    [1,3,5,1,6,1,5,3,1]
];
let groups4 = [
    [1,0,0,0,6,0,0,0,1],
    [1,1,1,1,1,1,1,1,1],
    [1,1,1,1,2,1,1,1,1],
    [1,1,1,1,2,1,1,1,1],
    [3,1,6,5,4,5,6,1,3]
];

/********La disposition des bricks au hasard au debut du jeu */
export let groups;
let r = getRandomInt(4); //un nobre random entre 0 et 3

switch (r) {
    case 0:
        groups = groups1;
        break;
    case 1:
        groups = groups2;
        break;
    case 2:
        groups = groups3;
        break;
    case 3:
        groups = groups4;
        break;
}

export let name = 1;
export let brickPadding = 20;
export let brickOffsetTop = 20;
export let brickOffsetLeft = 20;
export let brickWidth = 88;
export let brickHeight = 40;
let countBrick = 0;

//*** Fonction qui cree les briques ******** */
export function initbrick() {
    for (let i = 0; i < groups.length; i++) {
        for (let j = 0; j < groups[i].length; j++) {
            let brickX = (j * (brickWidth + brickPadding)) + brickOffsetLeft;
            let brickY = (i * (brickHeight + brickPadding)) + brickOffsetTop;
            if (groups[i][j] == 1) {    //1 = brique ordinaire
                let box = Bodies.rectangle(brickX + brickWidth / 2, brickY + brickHeight / 2, brickWidth, brickHeight, {
                    isStatic: true,
                    label: "brick",
                    render: {
                        sprite: {
                            texture: 'assets/image/brique/normal.png',
                            xScale: 1.5,
                            //yScale: 0.035
                        }
                    }
                });
                World.add(engine.world, [box]);
                countBrickUP();     //conmteur de briques ++ 
                                    //on compte les brique qui sont affichees
            } else if (groups[i][j] == 2) { // 2,3,4,5,6 = brique bonus
                let box = Bodies.rectangle(brickX + brickWidth / 2, brickY + brickHeight / 2, brickWidth, brickHeight, {
                    isStatic: true,
                    label: "brick",
                    render: {
                        sprite: {
                            texture: 'assets/image/brique/bonusball.png',
                            xScale: 1.5,
                            //yScale: 0.035
                        }
                    },
                    /* render: {
                         fillStyle: "#78B0AE"
                     },*/

                    events: "bonusball"  //type du bonus de la brique
                });
                World.add(engine.world, [box]);
                countBrickUP();
            } else if (groups[i][j] == 3) {
                let box = Bodies.rectangle(brickX + brickWidth / 2, brickY + brickHeight / 2, brickWidth, brickHeight, {
                    isStatic: true,
                    label: "brick",
                    render: {
                        sprite: {
                            texture: 'assets/image/brique/bonusscore.png',
                            xScale: 1.5,
                            //yScale: 0.035
                        }
                    },
                    events: "bonusscore"   
                });
                World.add(engine.world, [box]);
                countBrickUP();
            } else if (groups[i][j] == 4) {
                let box = Bodies.rectangle(brickX + brickWidth / 2, brickY + brickHeight / 2, brickWidth, brickHeight, {
                    isStatic: true,
                    label: "brick",
                    render: {
                        sprite: {
                            texture: 'assets/image/brique/malusscore.png',
                            xScale: 1.5,
                            //yScale: 0.035
                        }
                    },
                    events: "malusscore"
                });
                World.add(engine.world, [box]);
                countBrickUP();
            } else if (groups[i][j] == 5) {
                let box = Bodies.rectangle(brickX + brickWidth / 2, brickY + brickHeight / 2, brickWidth, brickHeight, {
                    isStatic: true,
                    label: "brick",
                    render: {
                        sprite: {
                            texture: 'assets/image/brique/bonustime.png',
                            xScale: 1.5,
                            //yScale: 0.035
                        }
                    },
                    events: "bonustime" 
                });
                World.add(engine.world, [box]);
                countBrickUP();
            } else if (groups[i][j] == 6) {
                let box = Bodies.rectangle(brickX + brickWidth / 2, brickY + brickHeight / 2, brickWidth, brickHeight, {
                    isStatic: true,
                    label: "brick",
                    render: {
                        sprite: {
                            texture: 'assets/image/brique/malustime.png',
                            xScale: 1.5,
                            //yScale: 0.035
                        }
                    },
                    events: "malustime"
                });
                World.add(engine.world, [box]);
                countBrickUP();
            }
        }
    }
}


/***    Fonctions pour le comptage des briques  ******** */
export function countBrickUP() {
    countBrick += 1;
}

export function countBrickDown() {
    countBrick -= 1;
}

export function returnCountBrick() {
    return countBrick;
}