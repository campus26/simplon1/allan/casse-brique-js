import {
    engine,
    render
} from "./game.js";

let ballstartX = 500;
let ballstartY = 510;
let ballradius = 15;
let nbball = 0;
let arrayball = [];
export let ball = Bodies.circle(ballstartX, ballstartY, ballradius);

//****  creation d'une ball *********** */
export function initball() {
    ball = Bodies.circle(ballstartX, ballstartY, ballradius, {

        render: {
            sprite: {
                texture: 'assets/image/ball/sun.png',
                xScale: 0.035,
                yScale: 0.035
            }
        }
    });
    ball.friction = 0;
    ball.frictionAir = 0;
    ball.restitution = 1;
    ball.frictionStatic = 0;
    ball.inverseMass = 1000000; //pour que la ball ne ralantie pas 
    ball.label = "ballon";
    Body.setInertia(ball, Infinity)

    Body.setVelocity(ball, {
        x: getRandomInt(),  //velocite et direction random au depart da la ball
        y: -getRandomInt()
    });
    Body.setAngularVelocity(ball, 0.05);    //rotation de la ball
    addBall();
    World.add(engine.world, [ball]);
    arrayball.push(ball);
    corectTraject(ball);
}

//*********  gestion du nombre de balls  ******
export function setBall() {
    nbball -= 1;
}

export function addBall() {
    nbball += 1;
}

export function getnbBall() {
    return nbball;
}

export function getArrayBall() {
    return arrayball;
}

// tableau de ballon -> recup le tableau de ballon afficher ds console -> ajouter ballon dans tableau quand creation -> remove dans le tableau un ballon qui passe en dessous

/**********     genere une velocité random pour la ball     ******** */
function getRandomInt() {
    for (let i = 0; i < 80; i++) {
        let r = Math.random() * 10;
        if (r >= 6 && r <= 7.5) {
            return r;
        }
    }

}

/*********  Corige la trajectoire de la ball    *********** */
function corectTraject(ball) {
    Matter.Events.on(engine, 'beforeUpdate', function () {
        //si la trajectoire de la ball trop verticale
        if (ball.velocity.y < 0.5 && ball.velocity.y > -0.5) {
            //console.log(ball.velocity);

            if (ball.velocity.y <= 0) {
                Body.setVelocity(ball, {
                    x: ball.velocity.x,
                    y: -5
                });
            //console.log("apres", ball.velocity);
            } else if (ball.velocity.y >= 0) {
                Body.setVelocity(ball, {
                    x: ball.velocity.x,
                    y: 5
                });
                //Body.applyForce(ball, {x:ball.position.x, y:ball.position.y}, {x: 0, y: 2});
                //console.log("apres", ball.velocity);
            }
        }
         //si la trajectoire de la ball trop horisontale
        if (ball.velocity.x < 0.5 && ball.velocity.x > -0.5) {
            //console.log(ball.velocity);

            if (ball.velocity.x <= 0) {
                Body.setVelocity(ball, {
                    x: -5,
                    y: ball.velocity.y
                });
                //console.log("apres", ball.velocity);
            }else if (ball.velocity.x >= 0) {
                Body.setVelocity(ball, {
                    x: 5,
                    y: ball.velocity.y
                });
                //console.log("apres", ball.velocity);
            }
        }
    });
}