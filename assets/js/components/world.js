import {
    engine
} from "./game.js";

//création de notre univers de jeu 'les murs'

export function initworldborder() {
    var ground = Bodies.rectangle(500, 615, 1000, 30, {
        isStatic: true
    });
    ground.restitution = 1;
    var leftwall = Bodies.rectangle(-15, 300, 30, 600, {
        isStatic: true
    });
    leftwall.restitution = 1;
    var rightwall = Bodies.rectangle(1020, 300, 30, 600, {
        isStatic: true
    });
    rightwall.restitution = 1;
    var topwall = Bodies.rectangle(500, -15, 1000, 30, {
        isStatic: true
    });
    topwall.restitution = 1;
    topwall.label = 'topwall';
    World.add(engine.world, [leftwall, rightwall, topwall]);
}