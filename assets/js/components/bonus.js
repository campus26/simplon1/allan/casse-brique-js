import { engine } from "./game.js";

//switch case qui vient verifier quel brick bonus a ete casser et vient creer un bonus correspondant

export function bonus(pair) {
    // console.log(pair.events);
    switch (pair.events) {
        case "bonusball":
            briquebonus(pair, "bonusball");
            break;
        case "bonusscore":
            briquebonus(pair, "bonusscore");
            break;
        case "malusscore":
            briquebonus(pair, "malusscore");
            break;
        case "bonustime":
            briquebonus(pair, "bonustime");
            break;
        case "malustime":
            briquebonus(pair, "malustime");
            break;
    }
}

//creation du bonus qui tombe avec en parametre la brique auquel il appartient ainsi que le nom du bonus

function briquebonus(pair, nom) {
    let bonusbrique = Bodies.rectangle(pair.position.x, pair.position.y, 40, 28, {
        isSensor: true,
        label: nom,
        render: {
            sprite: {
                texture: 'assets/image/brique/bonusdrop.png',
                xScale: 1.5,
                //yScale: 0.035
            }
        },
        force: {
            x: 0,
            y: 0.02
        },
        friction: 0,
        frictionAir: 0,
        frictionStatic: 0
    });
    //console.log(bonusbrique);
    World.add(engine.world, [bonusbrique]);
}
