// notre controller avec les functions que nous utilisons et notre function "jeu"

import {
    initbrick
} from "./components/brique.js";
import {
    deleteBrick
} from "./components/event.js";
import {
    drawbr
} from "./components/bar.js";
import {
    initball
} from "./components/ballon.js";
import {
    initworldborder
} from "./components/world.js";
import {
    gameOverBall
} from "./components/gameover.js";
import {
    gameEnter,
    rejouer
} from "./components/aside.js";
import {
    detecbonusbar
} from "./components/event.js";
import {
    startTimer
} from "./components/time.js";
import {
    fireBall
} from "./components/fire.js";


//pour lancer le jeu on fait appelle aux functions suivants

export function game() {
    startTimer();
    initbrick();
    initball();
    initworldborder();
    drawbr();
    deleteBrick();
    detecbonusbar();
    fireBall();
    gameOverBall();
}
gameEnter();
rejouer();